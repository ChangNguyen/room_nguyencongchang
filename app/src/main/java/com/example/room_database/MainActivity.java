package com.example.room_database;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.room_database.database.UserDatabase;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText edtUserName, edtAddress;
    Button btnAdd;
    RecyclerView recyclerView;
    private List<User> mListUser;
    private UserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AnhXa();
        adapter = new UserAdapter();
        mListUser = new ArrayList<>();
        adapter.setData(mListUser);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddUser();
            }
        });
    }



    private void AnhXa() {
        edtAddress = findViewById(R.id.EDT_ADDRESS);
        edtUserName = findViewById(R.id.EDT_USERNAME);
        btnAdd = findViewById(R.id.BTN_ADD_USER);
        recyclerView = findViewById(R.id.RCV_USER);
    }

    private void AddUser() {
        String strUserName = edtUserName.getText().toString().trim();
        String strAddress = edtAddress.getText().toString().trim();
        if (TextUtils.isEmpty(strUserName) || TextUtils.isEmpty(strAddress)){
            return;
        }

        User user = new User(strUserName,strAddress);
        UserDatabase.getInstance(this).userDAO().insertUser(user);
        Toast.makeText(this, "Thanh Cong", Toast.LENGTH_SHORT).show();

        edtUserName.setText("");
        edtAddress.setText("");
        HideKeyBoard();

        mListUser =  UserDatabase.getInstance(this).userDAO().getListUser();
        adapter.setData(mListUser);

    }

    private void HideKeyBoard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }catch (NullPointerException ex){
            ex.printStackTrace();
        }
    }
}